import { Match, mergeProps, onCleanup, Switch } from "solid-js";
import { useState } from "./state";
import { StartTask, TaskList } from "./task";

const initState =
{
  guiState: "start screen",
  resources:
    [
      { resName: "gold", quant: 100 },
      { resName: "metal", quant: 0 },
      { resName: "water", quant: 0 },
      { resName: "crystal", quant: 0 },
    ],
  items: [],
  freshId: 0,
  tasks: []
}

const MainGui = () => {
  // set up store
  const [store, setStore] = useState()

  // tick update function
  const tick = () => setStore("tasks", t => t.count < t.info.time, "count", c => c + 1)
  const interval = setInterval(tick, 1000)
  onCleanup(() => clearInterval(interval))

  const caveTaskInfo =
  {
    name: "clean cave",
    time: 12,
    cost: [{ resName: "gold", quant: 30 }],
    award: [{ resName: "gold", quant: 50 }],
    anima: ["dig1", "dig2", "dig3", "dig4", "dig5", "dig6", "dig7", "dig8"]
  }

  return (
    <Switch fallback={<div>error</div>}>
      <Match when={store.guiState === "start screen"}>
        <button onClick={() => setStore("guiState", "main")}> start game </button>
      </Match>
      <Match when={store.guiState === "main"}>
        <div>
          <ShowResources />
          <StartTask taskInfo={caveTaskInfo} />
          <TaskList />
        </div>
        <ClearStore />
      </Match>
    </Switch>
  )
}

const ShowResources = () => {
  // set up store
  const [store, setStore] = useState()
  return (
    <For each={store.resources}>
      {res =>
        <div> {res.resName} {res.quant} </div>
      }
    </For>
  )
}

const ClearStore = () => {
  const [store, setStore] = useState()
  const clearStore = () => {
    setStore(s => initState)
  }
  return <button onClick={clearStore}> !!! erase ALL DATA !!!</button>
}

export { MainGui, initState }