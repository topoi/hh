import { render } from 'solid-js/web';
import { MainGui, initState } from './main';
import { StateProvider } from './state';

const App = () =>
  <StateProvider initState={initState}>
    <MainGui />
  </StateProvider>

render(App, document.getElementById('root'));
