import { createEffect, useContext, createContext } from "solid-js";
import { createStore } from "solid-js/store"

const createLocalStore = (initState) => {
  const [state, setState] = createStore(initState);
  if (localStorage.tasks) setState(JSON.parse(localStorage.tasks));
  createEffect(() => (localStorage.tasks = JSON.stringify(state)));
  return [state, setState];
}

const stateContext = createContext()
const StateProvider = (props) => {

  const [store, setStore] = createLocalStore(props.initState)

  return (
    <stateContext.Provider value={[store, setStore]}>
      {props.children}
    </stateContext.Provider>
  )
}
const useState = () => useContext(stateContext)

export { StateProvider, useState }