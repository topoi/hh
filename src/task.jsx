import { Match, Switch } from "solid-js";
import { useState } from "./state";

const enoughRes = (res, cost) =>
  cost.every(c => {
    const rres = res.find(r => c.resName === r.resName)
    if (rres) {
      return (rres.quant >= c.quant)
    }
    else {
      return (c.quant === 0)
    }
  })

const lacks = (res, cost) =>
  cost.map(c => {
    const rres = res.find(r => c.resName === r.resName)
    if (rres) {
      return { resName: c.resName, quant: (c.quant - rres.quant) }
    }
  }).filter(l => l.quant > 0)

const lefts = (res, cost) =>
  res.map(r => {
    const cres = cost.find(c => c.resName === r.resName)
    if (cres) {
      return { resName: r.resName, quant: (r.quant - cres.quant) }
    }
    else {
      return r
    }
  })

const awarded = (res, aw) =>
  res.map(r => {
    const awr = aw.find(a => a.resName === r.resName)
    if (awr) {
      return { resName: r.resName, quant: (r.quant + awr.quant) }
    }
    else {
      return r
    }
  })

// component for create new task button
const StartTask = (props) => {
  const [store, setStore] = useState()
  // state update for operate
  const taskInfo = props.taskInfo
  const addTask = () =>
    setStore(s => (
      { guiState: s.guiState, resources: lefts(s.resources, taskInfo.cost), freshId: s.freshId + 1, tasks: [...s.tasks, { id: s.freshId, info: taskInfo, count: 0 }] }
    ))
  return (
    <div>
      <button onClick={addTask} disabled={!enoughRes(store.resources, taskInfo.cost)}> Clean Cave </button>
      <Show when={!enoughRes(store.resources, taskInfo.cost)}>
        <For each={lacks(store.resources, taskInfo.cost)}>
          {r =>
            <div>!! lacks {r.resName} : {r.quant}</div>
          }
        </For>
        <div>need more res to excute</div>
      </Show>
    </div>
  )
}

// component for displaying existing tasks
const TaskList = () => {
  // state update for operate
  const [store, setStore] = useState()

  const newTask = task => {
    const finishTask = () => {
      setStore("tasks", ts => ts.filter(t => t.id != task.id))
      setStore("resources", s => awarded(s, task.info.award))
    }
    return (
      <Switch fallback={<div>err</div>}>
        <Match when={task.count >= task.info.time}>
          <div>Mission complete</div>
          <button onClick={finishTask}> Finish & Get Award </button>
        </Match>
        <Match when={task.count < task.info.time}>
          <div> task {task.info.name} time left: {task.info.time - task.count} </div>
          <div> {task.info.anima[Math.min(task.count, task.info.anima.length - 1)]} </div>
        </Match>
      </Switch>
    )
  }

  return (
    <>
      <For each={store.tasks}>
        {newTask}
      </For>
    </>
  )
}

export { StartTask, TaskList }